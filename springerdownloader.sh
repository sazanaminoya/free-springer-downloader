#!/bin/bash

# https://note.com/sangmin/n/nea8581fd10b4
# https://docs.google.com/spreadsheets/d/11a9jpJRBnuQ-TCEq2BzvSgFQT4DeOoIJKYCk_wXfYFw/edit#gid=661262745

# variable
needs_epub=false
output_dir_pdf="./output/pdf/"
output_dir_epub="./output/epub/"

# const
extention_pdf=".pdf"
extention_epub=".epub"
url_hat_pdf_1="https://link.springer.com/content/pdf/10.1007%2F"
url_hat_pdf_2="https://link.springer.com/content/pdf/10.1057%2F"
url_hat_epub="https://link.springer.com/download/epub/10.1007%2F"
lst_not_free=(978-3-319-32185-1,978-3-030-19128-3)
lst_needs_url_hat_pdf_2=(978-1-349-95348-6)

mkdir -p "${output_dir_pdf}"
mkdir -p "${output_dir_epub}"

while read row; do

  url_individual=`echo ${row} | cut -d , -f 1`
  isbn=`echo ${row} | cut -d , -f 2`

  if `echo "${lst_not_free[@]}" | grep -q "${url_individual}"`; then
    continue
  fi

  url_hat_pdf="${url_hat_pdf_1}"
  if `echo "${lst_needs_url_hat_pdf_2[@]}" | grep -q "${url_individual}"`; then
    url_hat_pdf="${url_hat_pdf_2}"
  fi

  curl -L "${url_hat_pdf}${url_individual}${extention_pdf}" -o "${output_dir_pdf}${isbn}${extention_pdf}"
  if "${needs_epub}"; then
    curl -L "${url_hat_epub}${url_individual}${extention_epub}" -o "${output_dir_epub}${isbn}${extention_epub}"
  fi
done < lst.csv

#https://link.springer.com/content/pdf/10.1057%2F
#https://link.springer.com/content/pdf/10.1007%2F
#curl -L https://link.springer.com/content/pdf/10.1007%2Fb100747.pdf -o "978-0-306-48048-5.pdf"
#https://link.springer.com/download/epub/10.1007%2F
#curl -L https://link.springer.com/download/epub/10.1007%2F978-0-387-21736-9.epub -o "978-0-306-48048-5.epub"